<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_seeallmagazine</name>
   <tag></tag>
   <elementGuidId>071fb351-02f6-46cb-a181-212c58b4bea1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-primary btn-md btn-round-30']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;portlet_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_G9y8KGsLVhSJ&quot;]/div/div[3]/div/div/div/div/div[2]/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
