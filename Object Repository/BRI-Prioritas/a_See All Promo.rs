<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_See All Promo</name>
   <tag></tag>
   <elementGuidId>f7aa0e35-4b1b-42d0-898f-0ace2f373161</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='portlet_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_DCFMVC2NvY5R']/div/div[3]/div/section/div/div[3]/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-holder-prioritas.btn-primary.btn-md.btn-round-30</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/web/bri-prioritas/promo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-holder-prioritas btn-primary btn-md btn-round-30</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>See All Promo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;portlet_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_DCFMVC2NvY5R&quot;)/div[@class=&quot;portlet-content&quot;]/div[@class=&quot;portlet-content-container&quot;]/div[@class=&quot;portlet-body&quot;]/section[@class=&quot;section-latest-promotion cx-section-latest-promotion-priotas section-background-prioritas&quot;]/div[@class=&quot;container CX-wrapper&quot;]/div[@class=&quot;btn-holder with-line text-center mt-5 aos-init aos-animate&quot;]/div[@class=&quot;box-btn&quot;]/a[@class=&quot;btn btn-holder-prioritas btn-primary btn-md btn-round-30&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='portlet_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_DCFMVC2NvY5R']/div/div[3]/div/section/div/div[3]/div/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'See All Promo')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Special Offer 5% off at OSIM with BRI Infinite and BRI Prioritas'])[2]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Head Office BRI'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='See All Promo']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/web/bri-prioritas/promo')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div[3]/div/a</value>
   </webElementXpaths>
</WebElementEntity>
