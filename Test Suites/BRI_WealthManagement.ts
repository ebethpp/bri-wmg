<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BRI_WealthManagement</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4424cc7b-dcad-4416-8437-f6418d855bb9</testSuiteGuid>
   <testCaseLink>
      <guid>23c02d7b-fe95-4736-a969-6d21d928b488</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BRIWMG</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a2010df-29b9-4c49-be63-3a31631228cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BRIPrioritas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e433a4fb-2931-4124-a4c2-077e8151311c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BRIPrivate</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
