import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.briwmg)

WebUI.maximizeWindow()

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('BRI-Prioritas/menu_briprioritas'))

WebUI.scrollToElement(findTestObject('BRI-Prioritas/a_See All Promo'), 30)

WebUI.click(findTestObject('Object Repository/BRI-Prioritas/a_See All Promo'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/BRI-Prioritas/a_See Promo'))

WebUI.navigateToUrl(GlobalVariable.briwmg)

WebUI.click(findTestObject('BRI-Prioritas/menu_briprioritas'))

WebUI.delay(5)

//WebUI.waitForElementClickable(findTestObject('btn_seeallmagazine'), 30)
WebUI.enhancedClick(findTestObject('btn_seeallmagazine'))

WebUI.delay(5)

//WebUI.scrollToPosition(300, 400)
WebUI.click(findTestObject('BRI-Prioritas/seemagazine'))

WebUI.delay(5)

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToElement(findTestObject('BRI-Prioritas/btn_findoutmoredebitcard'), 30)

WebUI.enhancedClick(findTestObject('BRI-Prioritas/btn_findoutmoredebitcard'))

WebUI.delay(5)

WebUI.back()

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/menu_product'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/prioritas_productdetailsinvestment'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/product_mutualfund'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/menu_product'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/bancassurancedetails'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/product_dasetera'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/menu_outlet'))

WebUI.setText(findTestObject('BRI-Prioritas/field_searchlocation'), 'Bandung')

WebUI.delay(3)

WebUI.click(findTestObject('BRI-Prioritas/btn_search'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/menu_insight'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/menu_program'))

WebUI.delay(5)

WebUI.click(findTestObject('BRI-Prioritas/program_SRO14'))

WebUI.delay(5)

WebUI.closeBrowser()

